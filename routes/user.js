const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user');

router.get('/StaticUsers', userCtrl.getStaticUsers );

router.get('/users/:id', userCtrl.getUser);

router.get('/users', userCtrl.getAllUsers);

module.exports = router;

