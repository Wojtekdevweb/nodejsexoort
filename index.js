const app = require('./app');
const port = process.env.PORT || 3000;

const mongoose = require('mongoose');
const { DB_NAME, DB_HOST, DB_USER, DB_PWD } = process.env;
const dbUrl = `mongodb+srv://${DB_USER}:${DB_PWD}@${DB_HOST}/${DB_NAME}?retryWrites=true&w=majority`;

// Connexion à la BDD...
mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log(`Connexion à la bdd ${DB_NAME} ok.`))
  .catch(e => console.log(e))

// app.listen(PORT, () => console.log(`Listening on port ${PORT}.`));
app.listen(port, () => console.log(`Server running at ${port}`));