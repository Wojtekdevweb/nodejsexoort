const User = require('../models/user');


// Get static JSON USERS
function getStaticUsers(req, res) {
  const users = require('../fixtures');
  res.status(200).json({ users })
}

// Get User By ID From MongoDB
function getUser(req, res) {
  const userId = req.params.id;
  User.findById(userId, (err, user) => {
    if (err || !user) {
      return res.status(404).send({
        message: "L'utilisateur n'existe pas"
      });
    }
    res.status(200).json({ user })
  })
}

// Get All Users From MongoDB
function getAllUsers(req, res) {
  User.find((err, user) => {
    if (err || !user) {
      return res.status(404).send({
        message: "L'utilisateur n'existe pas"
      });
    }
    res.status(200).json({ user })
  })
}


module.exports = {
  getStaticUsers,
  getUser,
  getAllUsers
}
